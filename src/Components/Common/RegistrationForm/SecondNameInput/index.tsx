import React, { FC } from 'react';
import style from './SecondNameInput.module.scss';

const SecondNameInput: FC = () => {
    return (
        <>
            <p className={style.description_input}>Ваша фамилия</p>
            <input type="text" className={style.input} />
        </>
    );
};

export default SecondNameInput;