import React, { FC } from 'react';
import style from './RepeatPassword.module.scss';

const RepeatPassword: FC = () => {
    return (
        <>
            <p className={style.description_input}>Повторите пароль:</p>
            <input type="password" className={style.input}/>
        </>
    );
};

export default RepeatPassword;