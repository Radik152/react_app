import React, { FC } from 'react';
import style from './NameInput.module.scss';

const NameInput: FC = () => {
    return (
        <>
            <p className={style.description_input}>Ваше имя</p>
            <input type="text" className={style.input} />
        </>
    );
};

export default NameInput;