import React, { FC } from 'react';
import style from './RegistrationButton.module.scss';

const RegistrationButton: FC = () => {
    return (
        <>
            <input type="submit" className={style.button_input} value="Зарегистрироваться"/>
        </>
    );
};

export default RegistrationButton;