import React, { FC } from 'react';
import style from './DateInput.module.scss';

const DateInput: FC = () => {
    return (
        <>
            <p className={style.description_input}>Дата Рождения</p>
            <input type="date" className={style.input} value="23.03.2021"/>
        </>
    );
};

export default DateInput;