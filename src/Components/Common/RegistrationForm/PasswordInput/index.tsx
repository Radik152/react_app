import React, { FC } from 'react';
import style from './PasswordInput.module.scss';

const PasswordInput: FC = () => {
    return (
        <>
            <p className={style.description_input}>Введите пароль:</p>
            <input type="password" className={style.input}/>
        </>
    );
};

export default PasswordInput;