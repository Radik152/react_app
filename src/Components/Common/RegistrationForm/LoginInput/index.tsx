import React, { FC } from 'react';
import style from './LoginInput.module.scss';

const LoginInput: FC = () => {
    return (
        <>
            <p className={style.description_input}>Введите логин:</p>
            <input type="text" className={style.input} />
        </>
    );
};

export default LoginInput;