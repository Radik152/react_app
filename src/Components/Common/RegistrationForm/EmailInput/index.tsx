import React, { FC, useState } from 'react';
import style from './EmailInput.module.scss';

interface IProps {
    setEmail: any
};

const EmailInput: FC<IProps> = (props:IProps) => {
    const { setEmail } = props;
    const [value,setValue] = useState<string>('');
    return (
        <>
            <p className={style.description_input}>Введите email:</p>
            <input type="text" className={style.input} value={value} onChange={(e)=>{setValue(e.currentTarget.value)}}  />
        </>
    );
};

export default EmailInput;