import React, { FC, useEffect } from "react"
import style from "./TextInput.module.scss"

interface IProps {
    setLogin: any
}

 
const TextInput:FC<IProps> = (props:IProps) => {
    const { setLogin } = props;
    // useEffect (()=>{
    //     console.log("component mount");
    //     let timerId;
    //     timerId = setTimeout(()=> console.log('Timer done'),3000);
    // },[]);

    return(
        <>
            <input className={style.input} 
            type="text" 
            placeholder="Логин"
            onChange={(e)=>console.log(e.currentTarget.value)}/>
        </>
    )
}

export default TextInput;