import React, { FC } from "react"
import style from "./RegistrationButton.module.scss"


const RegistrationButton: FC = () => {
    return(
        <>
            <input 
            className={style.registration_button}
            type="button" 
            value="Зарегистрироваться"
            />
        </>
    )
}

export default RegistrationButton;