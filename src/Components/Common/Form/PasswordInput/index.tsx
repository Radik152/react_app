import React, { FC } from "react"
import style from "./PasswordInput.module.scss"

interface IProps {
    setPassword: any
}

const PasswordInput:FC<IProps> = (props:IProps) => {
    const { setPassword } = props;
    return(
        <>
            <input className={style.input} 
            type="password" 
            placeholder="Пароль"
            onChange={(e)=>console.log(e.currentTarget.value)}/>
        </>
    )
}

export default PasswordInput;