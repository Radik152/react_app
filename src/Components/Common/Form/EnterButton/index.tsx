import React, { FC } from "react"
import style from "./EnterButton.module.scss"


const EnterButton: FC = () => {
    return(
        <>
            <input
            className={style.enter_button} 
            type="button" 
            value="Войти"/>
        </>
    )
}

export default EnterButton;