import React from "react";
import style from "./HeaderUl.module.scss";

class HeaderUl extends React.PureComponent {
  render() {
    return (
      <>
        <div className={style.header__wrapper}>
              <ul className={style.header__menu__list}>
                {/* <li><a href={}>Главная</a></li>
                <li><a href={}>О нас</a></li>
                <li><a href={}>Наши преимущества</a></li>
                <li><a href={}>Контакты</a></li> */}
                <li>Главная</li>
                <li>О нас</li>
                <li>Наши преимущества</li>
                <li>Контакты</li>
                <li>Профиль</li>
              </ul>
        </div>
      </>
    );
  }
}

export default HeaderUl;