import React from "react";
import style from "./Header.module.scss";
import HeaderUl from "./HeaderUl";
import HeaderRightMenu from "./HeaderRightMenu";
import logo from "../../assets/image/Header/logo.png";

class Header extends React.PureComponent {
  render() {
    return (
      <>
        <div className={style.header__wrapper}>
          <div className={style.container}>
            <div className={style.header__content}>
              <div className={style.left__header__menu}>
                <img src={logo} alt="" className={style.logo__header} />
                <HeaderUl />
              </div>
              <HeaderRightMenu />
            </div>
          </div>
        </div>
      </>
    );
  };
};

export default Header;
