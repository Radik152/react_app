import React from "react";
import style from "./HeaderRightMenu.module.scss";


class HeaderRightMenu extends React.PureComponent {
  render() {
    return (
      <>
        <div className={style.right__header__information}>
          <span className={style.header__phone}>8 (831) 416-16-67</span>
          <span className={style.header__address}>Прос-т Гагарина, д.231</span>
        </div>
      </>
    );
  }
}

export default HeaderRightMenu;
