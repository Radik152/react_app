import React from "react";
import style from "./FirstColumnFooter.module.scss";

class FirstColumnFooter extends React.PureComponent {
  render() {
    return (
      <>
        <div className={style.first__column__footer}>
            <p>© - IT Events, 2017</p>
            <p>Пользовательское соглашение</p>
            <p>Документы</p>
        </div>
      </>
    );
  }
}

export default FirstColumnFooter;