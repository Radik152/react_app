import React from "react";
import style from "./Footer.module.scss";
import FirstColumnFooter from "./FirstColumnFooter";
import SecondColumnFooter from "./SecondColumnFooter";
import ThirdColumnFooter from "./ThirdColumnFooter";

class Footer extends React.PureComponent {
  render() {
    return (
      <>
        <div className={style.footer__wrapper}>
             <div className={style.container}>
                <div className={style.footer__content}>
                    <FirstColumnFooter />
                    <SecondColumnFooter />
                    <ThirdColumnFooter />
                </div>
             </div>
        </div>
      </>
    );
  }
}

export default Footer;
