import React from "react";
import style from "./SecondColumnFooter.module.scss";

class SecondColumnFooter extends React.PureComponent {
  render() {
    return (
      <>
        <div className={style.second__column__footer}>
            <ul className={style.menu__footer}>
                <li>Главная</li>
                <li>О нас</li>
                <li>Наши преимущества</li>
                <li>Контакты</li>
            </ul>
        </div>
      </>
    );
  }
}

export default SecondColumnFooter;