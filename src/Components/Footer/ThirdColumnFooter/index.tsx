import React from "react";
import style from "./ThirdColumnFooter.module.scss";
import logoVk from "../../../assets/image/Footer/vk.png";
import logoInst from "../../../assets/image/Footer/instagram.png";
import logoYoutube from "../../../assets/image/Footer/youtube.png";

class ThirdColumnFooter extends React.PureComponent {
  render() {
    return (
      <>
        <div className={style.third__column__footer}>
            <p>Мы в социальных сетях</p>
            <div className={style.img__social}>
                <img src={logoVk} alt=""/>
                <img src={logoInst} alt=""/>
                <img src={logoYoutube} alt=""/>
            </div>
        </div>
      </>
    );
  }
}

export default ThirdColumnFooter;