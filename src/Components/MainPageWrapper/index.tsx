import React from "react";
import Header from "../Header";
import style from "./MainPageWrapper.module.scss";
import Footer from "../Footer";
import MainPage from "../MainPage";

class MainPageWrapper extends React.PureComponent {
  render() {
    return (
      <>
        <div className={style.main_page_wrapper}>
          <Header />
          <MainPage />
          <Footer />
        </div>
      </>
    );
  }
}

export default MainPageWrapper;
