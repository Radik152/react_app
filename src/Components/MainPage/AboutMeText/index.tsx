import React from "react";
import style from "./AboutMeText.module.scss";

class AboutMeText extends React.PureComponent {
  render() {
    return (
      <>
        <div className={style.content__about__me}>
            <h2 className={style.second__title__about}>О нас</h2>
            <p className={style.text_about}>Создаем автоматизированные системы управления, применяя передовые методологии и современные технологии разработки программного обеспечения. Обеспечиваем полный цикл разработки и сопровождения программного продукта: сбор требований и анализ задачи, дизайн и разработка, интеграция со сторонними продуктами и сервисами, обучение и сопровождение.</p>
        </div>
      </>
    );
  }
}

export default AboutMeText;