import React from "react";
import style from "./MainPage.module.scss";
import imgAboutMe from "../../assets/image/MainPage/about_me.jpg";
import AboutMeText from "./AboutMeText";

class MainPage extends React.PureComponent {
  render() {
    return (
      <>
            <div className={style.container}>
                <div className={style.about__me}>
                    <img src={imgAboutMe} alt=""  className={style.img__about__me}/>
                    <AboutMeText />
                </div>
            </div>
      </>
    );
  }
}

export default MainPage;
