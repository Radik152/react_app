import React, { FC } from 'react';
import style from "./AuthPage.module.scss";
import Header from "../Header";
import Footer from "../Footer";
import AuthForm from "./AuthForm";

const AuthPage: FC = () => {
    return(
        <>
        <div className={style.auth_page_wrapper}>
            <Header />
            <AuthForm />
            <Footer />
        </div>
        </>
    );
};

export default AuthPage