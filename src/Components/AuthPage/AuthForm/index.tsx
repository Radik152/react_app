import React, { FC, useEffect, useState } from 'react';
import style from "./AuthForm.module.scss";
import TextInput from "../../Common/Form/TextInput";
import PasswordInput from "../../Common/Form/PasswordInput";
import RegistrationButton from "../../Common/Form/RegistrationButton";
import EnterButton from "../../Common/Form/EnterButton";
import ForgotPassword from "../../Common/Form/ForgotPassword";

const AuthForm:FC = () => {
    const [login,setLogin] = useState<string>("");
    const [password,setPassword] = useState<string>("");
    const [isFormOpen,setisFormOpen] = useState<boolean>(false)

    // useEffect (()=>{
    //     console.log("component mount");
    // },[]);

    // useEffect (()=>{
    //     console.log("component mount");
    // },[login]);

    return (
        <>
            <div className={style.auth_form_wrapper}>
                <form  className={style.form}>
                    <h1>Авторизация</h1>
                    {/* <button type='button' 
                    onClick={()=>setisFormOpen(!isFormOpen)}>
                    {isFormOpen ? "Hide": "Show"}
                   </button>  */}
                    <TextInput setLogin={setLogin}/>
                    <PasswordInput setPassword={setPassword}/>
                    <div className={style.buttons_form}>
                        <RegistrationButton />
                        <EnterButton />
                    </div>
                    <ForgotPassword />
                   
                </form>
            </div>
        </>
    )
}

export default AuthForm;