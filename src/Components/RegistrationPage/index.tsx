import React, { FC } from 'react';
import style from './RegistrationPage.module.scss';
import Header from "../Header";
import Footer from "../Footer";
import RegistrationForm from "./RegistrationForm";

const RegistrationPage: FC = () => {
    return (
        <>
            <Header />
            <RegistrationForm />
            <Footer />
        </>
    )
}

export default RegistrationPage;