import React, { FC, useState, useEffect } from "react"
import style from "./RegistrationForm.module.scss"
import NameInput from "../../Common/RegistrationForm/NameInput";
import SecondNameInput from "../../Common/RegistrationForm/SecondNameInput";
import DateInput from "../../Common/RegistrationForm/DateInput";
import LoginInput from "../../Common/RegistrationForm/LoginInput";
import EmailInput from "../../Common/RegistrationForm/EmailInput";
import PasswordInput from "../../Common/RegistrationForm/PasswordInput";
import RepeatPassword from "../../Common/RegistrationForm/RepeatPassword";
import RegistrationButton from "../../Common/RegistrationForm/RegistrationButton";

const RegistrationForm: FC = () => {
    const [count,setCount] = useState<number>(0);
    const [email,setEmail] = useState<string>("");
    useEffect(()=> {
        document.title = `Вы нажали ${count} раз`;
    })

    return (
        <>
            <div className={style.registration_form_container}>
                <form className={style.registration_form} onSubmit={()=>{setEmail('')}}>
                    <h1 className={style.title_registration_form}>Регистрация</h1>
                    <NameInput />
                    <SecondNameInput />
                    <DateInput />
                    <LoginInput />
                    <EmailInput setEmail={setEmail}/>
                    <PasswordInput />
                    <RepeatPassword />
                    <RegistrationButton />
                    <input type="button"
                    onClick={()=>setCount(count+1)} 
                    value="Click!"/>
                    <p>Вы нажали {count} раз</p>
                </form>
            </div>
        </>
    );
}

export default RegistrationForm;